import csv
import os
import re
import pandas as pd
from tika import parser
from tkinter import filedialog as fd


# fonction qui lit les fichiers du corpus et qui génère un fichier FinalCorpus.txt netoyés
def check(location, filestorage):
	CorpusNetoye = open(filestorage + "/FinalCorpus.txt", "a+")
	dir = os.listdir(location)
	print(location)
	for fileRrDir in dir:
		if os.path.isfile(location + fileRrDir):
			file = location + fileRrDir
			file_content = parsing(file)
			# fonction netoyage a revoir
			# file_content = netoyage(file_content)
			CorpusNetoye.write(''.join(file_content).strip() + "\n")
		elif os.path.isdir(location + fileRrDir):
			check((location + fileRrDir + "/"), filestorage)
	CorpusNetoye.close()


# Fonction pour fusionner des fichiers csv du meme format pour
# avoir comme résultat un fichier concaténé pour la verification
def fusionFichersCsv():
	files = fd.askopenfilenames(
		title="Veillez selectionner des fichiers csv contenant des sigles sur la premiere colonne")
	if len(files) > 1:
		df = pd.concat([pd.read_csv(f, delimiter=',', encoding='UTF-8') for f in files])
	else:
		df = pd.read_csv(files[0])
	df.to_csv("../annex/fusionCsv.csv", index=False)
	return df


# Verification dans le fichier contenant des sigles : (Condition le nom des sigles sont la premiere colonne)
def check_ORG_In_Sigles(filename, content) -> bool:
	with open("../" + filename) as f:
		print(filename)
		reader = csv.reader(f, skipinitialspace=True, delimiter=',', quoting=csv.QUOTE_NONE)
		reader.__next__()
		for row in reader:
			print(row[0])
			if content == row[0]:
				return True
		return False


# fonction de parsing de fichier
def parsing(filename):
	try:
		parsed = parser.from_file(filename)["content"]
		if parsed is None:
			return " "
		else:
			return parsed
	except:
		print("Erreur sur le chargement du fichier " + filename)
	finally:
		print("The 'try except' is finished")


# Fonction de netoyage du corpus
def netoyage(filecontent):
	exp_url = "(<?( )?https?:\/\/(www\.)?[-A-Za-zÀ-ÖØ-öø-ÿ0-9@:%._\+~#=]{1,256}\.[A-Za-zÀ-ÖØ-öø-ÿ0-9()]{1,6}\b([-A-Za-zÀ-ÖØ-öø-ÿ0-9()@:%_\+\.\/~#?&=]*)( )?>?)"

	exp_number = "([0-9]+(\/|\.)[0-9]+(\/|\.)[0-9]+)|((([0-9]+(\.| |:)){2,})[0-9]+)|((\n){3,})"
	# Elimination des tout type de numéro heures
	re.sub(exp_number, ' ', ''.join(filecontent))

	exp_backslash = "(\n)*"
	# Elimination des retours a la ligne en gardant qu'un seul
	re.sub(exp_backslash, '\n', filecontent)

	# Elimination des url
	re.sub(exp_url, "", filecontent)
	return filecontent
