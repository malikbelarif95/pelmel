# importation des bibliotéques
import os.path
import tkinter as tk
from tkinter import filedialog as fd, NO, CENTER, W
from tkinter import messagebox as mbox
from tkinter import ttk
from ttkthemes import ThemedTk
from datetime import datetime as d
import os
import re
import spacy
from CleanUP import clean
# Fin importation des Bibliothèque

# La class d'interface qui contient les widgets
class App(ThemedTk):
	# Constructeur pour les declaration des attributs
	def __init__(self):
		super().__init__()
		self.folder = ""

		# Variable d'expression réguliere
		self.reg = "(([A-Z][a-zÀ-ÖØ-öø-ÿ]{2,}(-([A-Z][a-zÀ-ÖØ-öø-ÿ]{2,}))? [A-Z]{2,}((-| )[A-Z][A-Za-zÀ-ÖØ-öø-ÿ]+)*)|([A-Z]{2,}((-| )[A-Z][A-Za-zÀ-ÖØ-öø-ÿ]+)? [A-Z][a-zÀ-ÖØ-öø-ÿ]{2,}(-([A-Z][a-zÀ-ÖØ-öø-ÿ]{2,}))?))"

		# Labels d'Affichage
		self.label = ttk.Label(text="Liste Terme", font=("Arial", 15), justify='center')
		self.label2 = ttk.Label(text="Elements ignorés ou supprimés", font=("Arial", 15), justify='center')
		self.label3 = ttk.Label(text="Elements valides", font=("Arial", 15), justify='center')

		# Declaration d'une la liste principale qui contient le premier traitement de spacy
		self.my_game = ttk.Treeview(padding=20, height=10)
		self.my_game['columns'] = ('ID', 'TERME')

		# Configuration des colonnes
		self.my_game.column("#0", width=0, stretch=NO)
		self.my_game.column("ID", anchor=CENTER, width=20)
		self.my_game.column("TERME", anchor=W, width=180)
		self.my_game.heading("#0", text="", anchor=CENTER)
		self.my_game.heading("ID", text="ID", anchor=CENTER)
		self.my_game.heading("TERME", text="TERME", anchor=CENTER)

		# Declaration de la liste qui contient des element supprimés ou ignorés
		self.tree_sup = ttk.Treeview(padding=30, height=10)
		self.tree_sup['columns'] = ('ID', 'TERME')

		# Configuration des colonnes
		self.tree_sup.column("#0", width=0, stretch=NO)
		self.tree_sup.column("ID", anchor=CENTER, width=20)
		self.tree_sup.column("TERME", anchor=W, width=180)
		self.tree_sup.heading("#0", text="", anchor=CENTER)
		self.tree_sup.heading("ID", text="ID", anchor=CENTER)
		self.tree_sup.heading("TERME", text="TERME", anchor=CENTER)

		# Declaration de la liste qui contient des elements valides
		self.tree_valid = ttk.Treeview(padding=30, height=10)
		self.tree_valid['columns'] = ('ID', 'TERME')

		# Configuration des colonnes
		self.tree_valid.column("#0", width=0, stretch=NO)
		self.tree_valid.column("ID", anchor=CENTER, width=20)
		self.tree_valid.column("TERME", anchor=W, width=180)

		self.tree_valid.heading("#0", text="", anchor=CENTER)
		self.tree_valid.heading("ID", text="ID", anchor=CENTER)
		self.tree_valid.heading("TERME", text="TERME", anchor=CENTER)

		# Variable pour savoir qu'elle entité je suis entrain de traité
		# showOrg pour les organismes
		# showPer pour les personnes
		self.showOrg = self.showPer = False

		# setter des information de l'interface pricipale
		self.title('PelMel')
		self.geometry('900x120')

		# Declaration et configuration des Buttons de la premiere partie de l'interface
		self.btn1 = ttk.Button(self, text="PreparationCorpus", width=30, command=self.functionBtn1_preparation, padding=30)

		self.btn1.grid(column=1, row=0, sticky='we')

		self.btn2 = ttk.Button(self, text="Visualisation", width=30, command=self.functionBtn2_visualisation, padding=30)
		self.btn2.state(["disabled"])

		self.btn2.grid(column=2, row=0, sticky='we')

		self.btn3 = ttk.Button(self, text="Traitement", width=30, command=self.ok_Or_Cancled, padding=30)
		self.btn3.state(["disabled"])

		self.btn3.grid(column=3, row=0, sticky='we')
		# fin de la configuration des Buttons

# fonction de button de validation unitaire d'un champ dans la liste pricipale
	def validation_unique(self):
		try:
			# Selection du champs selectionné
			selected_item = self.my_game.selection()[0]

			# Ecriture dans le fichier de validation pour personnes ou organisme
			if self.showPer:
				open(self.result_storage + "/element_validate_per.txt", "a+").write(
					self.my_game.set(selected_item, column="TERME") + "\n")
				self.Affiche_List_Valide()
			else:
				open(self.result_storage + "/element_validate_org.txt", "a+").write(
					self.my_game.set(selected_item, column="TERME") + "\n")
				self.Affiche_List_Valide()

			# Suppression de l'element de la liste pricipale
			self.deleteItem(selected_item)
		except:
			print("error validation unique")

	# déplacement du champ selectionné depuis la liste des elements valider vers la liste des elements supprimés
	def move_to_sup(self):
		try:
			selected_item = self.tree_valid.selection()[0]
			if self.showPer:
				open(self.result_storage + "/element_ignore_per.txt", "a+").write(
					self.tree_valid.set(selected_item, column="TERME") + "\n")
				self.AfficheElementIgnoresPersonne()
				self.Affiche_List_Valide()
			else:
				open(self.result_storage + "/element_ignore_org.txt", "a+").write(
					self.tree_valid.set(selected_item, column="TERME") + "\n")
				self.Affiche_List_Valide()
			self.moveItem_to_sup(selected_item)
		except:
			print("")
	# déplacement du champ selectionné depuis la liste des elements supprimés vers la liste des elements validés
	def move_to_valide(self):
		try:
			selected_item = self.tree_sup.selection()[0]
			if self.showPer:
				open(self.result_storage + "/element_validate_per.txt", "a+").write(
					self.tree_sup.set(selected_item, column="TERME") + "\n")
				self.AfficheElementIgnoresPersonne()
				self.Affiche_List_Valide()
			else:
				open(self.result_storage + "/element_validate_org.txt", "a+").write(
					self.tree_sup.set(selected_item, column="TERME") + "\n")
				self.Affiche_List_Valide()
			self.moveItem_to_valide(selected_item)
		except:
			print("error move to valide ")

	# Fonction de sauvgarde du fichier FinalCorpus.txt lors de la modification
	def enregistrer(self):
		message = mbox.askokcancel(message=" Voulez vous enregistrer les modification ? ")
		try:
			if message:
				CorpusNetoye = open(self.result_storage + "/FinalCorpus.txt", "w+")
				CorpusNetoye.write((self.T.get(index1='1.0', index2='end-1c')).strip())
				CorpusNetoye.close()
		except:
			print("erreur lors de la  sauvgarde")

	# Fonction de sauvgarde du fichier FinalCorpus.txt lors de la modification avec Controle-s
	# La raison pour que j'ai fait deux fonction identique c'est parque
	# pour sauvgarder avec <CTRL + s> l'interface demande qu'il y est aumoin un attribut "event=None" pour la fonction
	# marche normalement
	# c'est pour cela que j'ai des deux fonctions enregistrer et enreg identique
	def enreg(self, event=None):
		message = mbox.askokcancel(message="Voulez vous enregistrer les modification ? ")
		try:
			if message:
				CorpusNetoye = open(self.result_storage + "/FinalCorpus.txt", "w+")
				CorpusNetoye.write((self.T.get(index1='1.0', index2='end-1c')).strip())
				CorpusNetoye.close()
		except:
			print("erreur lors de la  sauvgarde")

	# Fonction pour le button visualisation qui permet d'aficher une interface pour voir et de modifier
	# le contenu du fichier FinalCorpus.txt
	def functionBtn2_visualisation(self):
		CorpusNetoye = open(self.result_storage + "/FinalCorpus.txt", "r")
		wind_corpus = tk.Tk()

		# Enregistrement lors de l'appuis sur CTRL + s
		wind_corpus.bind('<Control-s>', self.enreg)

		menubar = tk.Menu(wind_corpus)
		menu1 = tk.Menu(menubar, tearoff=0)
		menu1.add_command(label="Enregistrer", command=self.enregistrer)
		menubar.add_cascade(label="Fichier", menu=menu1)

		# Configuration deuxième fenêtre
		wind_corpus.geometry("500x800")
		S = tk.Scrollbar(wind_corpus)
		self.T = tk.Text(wind_corpus, height=800, width=500)
		self.T.insert(tk.END, CorpusNetoye.read())
		S.pack(side=tk.RIGHT, fill=tk.Y)
		self.T.pack(side=tk.LEFT, fill=tk.Y)
		S.config(command=self.T.yview)
		self.T.config(yscrollcommand=S.set)
		CorpusNetoye.close()
		wind_corpus.config(menu=menubar)


		# Fonction de verification de si ile personen est valide
	def verif_match_personne(self):

		# Expression régulière de generation des noms et prénoms de personnes
		reg = "((([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}((-| )([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}))? [A-Z]{2,}((-| )[A-Z][A-Za-zÀ-ÖØ-öø-ÿ-]+)*)|([A-Z]{2,}((-| )[A-Z][A-Za-zÀ-ÖØ-öø-ÿ]+)? [A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}((-| )([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,})+)?)|([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}(-| ))?([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}[A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}((-| )[A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,})*([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,})))|(([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,})( |-)[A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}))"
		# reg = "(([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}(-([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}))? [A-Z]{2,}((-| )[A-Z][A-Za-zÀ-ÖØ-öø-ÿ-]+)*)|([A-Z]{2,}((-| )[A-Z][A-Za-zÀ-ÖØ-öø-ÿ]+)? [A-Z][a-zÀ-ÖØ-öø-ÿ]{2,}(-([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}))?)|([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}-)?([A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,} [A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,}(-[A-Z][a-zÀ-ÖØ-öø-ÿ-]{2,})?))"
		file = open(self.result_storage + "/resultat_PER.txt", "r").readlines()
		file_Delete = open(self.result_storage + "/element_ignore_per.txt", "a+")
		file_Validate = open(self.result_storage + "/element_validate_per.txt", "a+")

		# Parcoure de boucle pour la verification de chaque phrase
		for line in file:
			trouve = re.findall(reg, line)
			if len(trouve) != 0:

				# Conversion vers un string
				string = ''.join(trouve[0][0])
				file_Validate.write(string.strip() + "\n")
			else:
				file_Delete.write(line.strip() + "\n")
		file_Validate.close()
		file_Delete.close()

		# Fonction de verification de si un organisme est valide
	def verif_match_organisme(self):
		file = open(self.result_storage + "/resultat_ORG.txt", "r").readlines()
		file_Delete = open(self.result_storage + "/element_ignore_org.txt", "a+")
		file_Validate = open(self.result_storage + "/element_validate_org.txt", "a+")

		# Vérification dans la liste de reference
		for line in file:
			trouve = clean.check_ORG_In_Sigles("annex/fusionCsv.csv", line.strip().upper())
			if trouve or len(re.split(" ", line)) > 1:
				file_Validate.write(line.strip() + "\n")
			else:
				file_Delete.write(line.strip() + "\n")

		file_Validate.close()
		file_Delete.close()

	# Fonction relieé au button de validation automatique.
	def select_bouton_valider(self):
		repons = mbox.askokcancel(message="Etes vous sur de valider ces resultats : Personnes et Organismes")
		if repons:
			if self.showPer:
				self.verif_match_personne()
				self.AfficheElementIgnoresPersonne()
			else:
				self.verif_match_organisme()
				self.AfficheElementIgnoresOrganisme()
			self.Affiche_List_Valide()

	# Fonction pour afficher la liste des elements valides
	def Affiche_List_Valide(self):
		if self.showPer:
			file_Validate = open(self.result_storage + "/element_validate_per.txt", "r")
		else:
			file_Validate = open(self.result_storage + "/element_validate_org.txt", "r")
		for item in self.tree_valid.get_children():
			self.tree_valid.delete(item)
		self.tree_valid.update()
		j = 0
		for i in file_Validate:
			self.tree_valid.insert(parent='', text='', index='end', iid=str(j), values=(j, i.strip()))
			j += 1

	# Fonction pour la preparation de corpus
	def functionBtn1_preparation(self):

		# pop up de validation ou de repus de preparation de corpus
		directory = fd.askdirectory()
		print(directory)
		if os.path.isdir(directory):
			self.folder = d.now().strftime("%m-%d-%Y_%H:%M:%S")
			print(os.getcwd())
			self.result_storage = os.getcwd() + "/" + self.folder
			os.mkdir(self.result_storage)
			clean.check((directory + "/"), self.result_storage)
			self.btn2.state(["!disabled"])
			self.btn3.state(["!disabled"])
		clean.fusionFichersCsv()

	# Fonction d'affichage de liste des organismes
	def AfficheListeOrganisme(self):
		print("per=", self.showPer)
		print("org=", self.showOrg)
		self.my_game.update()
		for item in self.my_game.get_children():
			self.my_game.delete(item)
		self.my_game.update()
		file = open(self.result_storage + "/resultat_ORG.txt", "r").readlines()
		j = 0
		for i in file:
			self.my_game.insert(parent='', text='', index='end', iid=str(j), values=(j, i.strip()))
			j += 1

		self.showOrg = True
		self.showPer = False

	# Fonction d'affichage de liste des personnes
	def AfficheListePersonne(self):
		print("per=", self.showPer)
		print("org=", self.showOrg)
		self.my_game.update()
		for item in self.my_game.get_children():
			self.my_game.delete(item)
		self.my_game.update()
		file = open(self.result_storage + "/resultat_PER.txt", "r").readlines()
		j = 0
		for i in file:
			self.my_game.insert(parent='', text='', index='end', iid=str(j), values=(j, i.strip()))
			j += 1

		self.showPer = True
		self.showOrg = False

	# Fonction d'affichage de liste des elements ignorés des personnes
	def AfficheElementIgnoresPersonne(self):
		for item in self.tree_sup.get_children():
			self.tree_sup.delete(item)

		file = open(self.result_storage + "/element_ignore_per.txt", "r").readlines()
		i = 0
		for line in file:
			self.tree_sup.insert(parent='', index='end', iid=str(i), values=(i, line.strip()))
			i += 1
		self.tree_sup.update()

	# Fonction d'affichage de liste des elements ignorés des organismes
	def AfficheElementIgnoresOrganisme(self):
		for item in self.tree_sup.get_children():
			self.tree_sup.delete(item)

		file = open(self.result_storage + "/element_ignore_org.txt", "r").readlines()
		i = 0
		for line in file:
			self.tree_sup.insert(parent='', index='end', iid=str(i), values=(i, line.strip()))
			i += 1
		self.tree_sup.update()

	# Fonction qui regroupe l'affichage des 3 listes pour les organismes
	# Liste des organismes
	# Liste des organismes valides
	# Liste des organismes ignorées
	def listORG(self):
		open(self.result_storage + "/element_ignore_org.txt", "a").close()
		open(self.result_storage + "/element_validate_org.txt", "a").close()
		if self.showPer:
			self.my_game.pack_forget()
		self.AfficheListeOrganisme()
		self.AfficheElementIgnoresOrganisme()
		self.Affiche_List_Valide()

	# Fonction qui regroupe l'affichage des 3 listes pour les personnes
	# Liste des personnes
	# Liste des personnes valides
	# Liste des personnes ignorées
	def listPER(self):
		open(self.result_storage + "/element_ignore_per.txt", "a").close()
		open(self.result_storage + "/element_validate_per.txt", "a").close()
		if self.showOrg:
			self.my_game.pack_forget()
		self.AfficheListePersonne()
		self.AfficheElementIgnoresPersonne()
		self.Affiche_List_Valide()

	# Fonction pour le traitement du corpus avec SpaCy
	#
	def ok_Or_Cancled(self):
		# Pop up de validation ou de refus de traitement
		repons = mbox.askokcancel(message="Etes Vous Sur de Traiter Ce Corpus !!")
		if repons:
			# Pop up d'informations
			mbox.showinfo(title="information", message=" Ceci peut prendre quelques minutes merci de patianter")
			self.spacy_function()
			self.geometry("1000x1200")
			# self.resizable(width=False, height=False)

			# Génération et configuration des widgets relié au traitement
			personne = ttk.Button(self, text="Personnes ", padding=30, width=30, command=self.listPER)
			personne.grid(column=2, row=1, ipady=5)

			organisation = ttk.Button(self, text="Organisation", padding=30, width=30, command=self.listORG)
			organisation.grid(column=2, row=2, ipady=5)

			self.btn_validation = ttk.Button(text="Validation Auto", width=15, padding=10, command=self.select_bouton_valider)
			self.btn_validation.grid(column=3, row=4, sticky="s")

			self.btn_del = ttk.Button(text="Supprimer", width=15, padding=10, command=self.deleteItemFromList)
			self.btn_del.grid(column=3, row=4)

			self.btn_valider = ttk.Button(text="Valider", width=15, padding=10, command=self.validation_unique)
			self.btn_valider.grid(column=3, row=4, sticky="n")

			self.btn_move_to_sup = ttk.Button(text="->", width=10, padding=10, command=self.move_to_sup)
			self.btn_move_to_sup.grid(column=2, row=6, sticky="w")

			self.btn_move_to_valid = ttk.Button(text="<-", width=10, padding=10, command=self.move_to_valide)
			self.btn_move_to_valid.grid(column=2, row=6, sticky="e")

	# Suppreesion d'un element donné comme paramétre item de la liste de personnes ou de organismes choisit
	def deleteItem(self, item):

		# Choix de liste
		if self.showPer:
			file = open(self.result_storage + "/resultat_PER.txt", "r")
			namefile = self.result_storage + "/resultat_PER.txt"
			update = 'PER'
		else:
			file = open(self.result_storage + "/resultat_ORG.txt", "r")
			namefile = self.result_storage + "/resultat_ORG.txt"
			update = 'ORG'
		lines = file.readlines()
		file.close()

		# Suppression du champs depuis la liste
		del lines[int(item)]

		# Génération d'un nouveau fichier est remplacé l'ancien par un nouveau sans les champs
		new_file = open(namefile, "w+")
		for line in lines:
			new_file.write(line)
		new_file.close()

		# Supression du champs depuis l'interface
		self.my_game.delete(item)

		# Mise à jour des elements de la liste
		self.my_game.update()
		if update == 'PER':
			self.AfficheListePersonne()
		else:
			self.AfficheListeOrganisme()

	# deplacement d'un element selectionné  depuis la liste validé vers la liste supprimé
	def moveItem_to_sup(self, item):
		if self.showPer:
			file = open(self.result_storage + "/element_validate_per.txt", "r")
			namefile = self.result_storage + "/element_validate_per.txt"
		else:
			file = open(self.result_storage + "/element_validate_org.txt", "r")
			namefile = self.result_storage + "/element_validate_org.txt"
		lines = file.readlines()
		file.close()
		del lines[int(item)]
		new_file = open(namefile, "w+")
		for line in lines:
			new_file.write(line)
		new_file.close()
		self.tree_valid.delete(item)
		self.tree_sup.update()
		if self.showPer:
			self.AfficheElementIgnoresPersonne()
		else:
			self.AfficheElementIgnoresOrganisme()

	# déplacement d'un element selectionné depuis la liste supprimé vers la liste validé
	def moveItem_to_valide(self, item):
		if self.showPer:
			file = open(self.result_storage + "/element_ignore_per.txt", "r")
			namefile = self.result_storage + "/element_ignore_per.txt"
		else:
			file = open(self.result_storage + "/element_ignore_org.txt", "r")
			namefile = self.result_storage + "/element_ignore_org.txt"
		lines = file.readlines()
		file.close()
		del lines[int(item)]
		new_file = open(namefile, "w+")
		for line in lines:
			new_file.write(line)
		new_file.close()
		self.tree_sup.delete(item)

		self.Affiche_List_Valide()
		self.AfficheElementIgnoresOrganisme()
		self.AfficheElementIgnoresPersonne()
		self.tree_valid.update()

	# Fonction de la selection d'un elements d'une liste et la suppresion par la suite
	# avec la fonction deleteItem
	def deleteItemFromList(self):
		selected_item = self.my_game.selection()[0]
		if self.showPer:
			open(self.result_storage + "/element_ignore_per.txt", "a+").write(
				self.my_game.set(selected_item, column="TERME") + "\n")
			self.AfficheElementIgnoresPersonne()
		else:
			open(self.result_storage + "/element_ignore_org.txt", "a+").write(
				self.my_game.set(selected_item, column="TERME") + "\n")
			self.AfficheElementIgnoresOrganisme()
		self.deleteItem(selected_item)

	# Fonction qui utilise la bibliotèque SpaCy pour le traitement du corpus
	def spacy_function(self):

		# liste pour le stockage et verifications  des occurences
		occurencePers = occurenceOrg = []

		# Configuration pour le pretraitemnt de SpaCy
		# Chargement du module pour l'apprentissage "fr_core_news_md" pour la lanque française
		nlp = spacy.load("fr_core_news_md")
		nlp.max_length = 100_000_000_000
		nlp.add_pipe('sentencizer')


		file = open(self.result_storage + "/FinalCorpus.txt", "r").readlines()
		result_pers = open(self.result_storage + "/resultat_PER.txt", "a+")
		result_org = open(self.result_storage + "/resultat_ORG.txt", "a+")
		reg = "(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)|([0-9]{1,2}( )?(h|H)( )?[0-9]{1,2})|(([0-9]{1,4}( )?(:|h|H|\/))+( ?[0-9]+)?)"
		for line in file:
			# Elémination des urls
			line = re.sub(reg, ' ', line)
			doc = nlp(line)
			for word in doc.ents:
				string = ''.join(word.text).strip()
				if word.label_ == "PER":
					if len(''.join(string).split(" ")) >= 2 and string not in occurencePers:
						result_pers.write(string + "\n")
						occurencePers.append(string)
				# Si le mot extrait est un organisme et que ça longeur est superieur a 1 oub ien il existe dans la liste de reference
				elif ((word.label_ == "ORG" and len(string) >= 2) or clean.check_ORG_In_Sigles("annex/fusionCsv.csv",
				                                                                               string.strip().upper())) and string not in occurenceOrg:
					result_org.write(string + "\n")
					occurenceOrg.append(string)
		# Setting et affichage des wdgets apres la fin de la fonction lors du traiemet du corpus
		self.label.grid(column=1, row=3, columnspan=2, sticky='we')
		self.label2.grid(column=3, row=5)
		self.my_game.grid(column=1, row=4, columnspan=2, sticky='we')
		self.tree_sup.grid(column=3, row=6)
		# self.lab = tk.Label(text="PelMel", bg="#F5FFFA", font=('Arial', 50, 'italic')).grid(column=2, row=0, rowspan=2,
		#                                                                                     columnspan=2, sticky="we")
		self.label3.grid(column=1, row=5)
		self.tree_valid.grid(column=1, row=6)

# Main du programe
if __name__ == "__main__":

	# Seclaration de l'interface
	app = App()

	# Configuration de theme
	# il existe plusieurs themes
	# disponibles sur le site
	# https://ttkthemes.readthedocs.io/en/latest/themes.html
	app.set_theme("adapta")

	# configuration de couleur du fond
	app.config(bg="#F5FFFA")

	app.option_add("*Label.Font", "helvetica 20 bold")
	# Affichage de l'interface
	app.mainloop()
