#!/bin/bash

echo $HOME
#Couleurs Pour les affichages
red=`tput setaf 1`
green=`tput setaf 2`
blue=`tput setaf 4`
res=`tput sgr0`

# Les pre-requis pour installer Anaconda
sudo apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6

# Vérification si le fichier est disponible pour ne pas refaire le script a chaque fois le téléchargement de anaconda prends 45 seconds avec une vitesse internet normal
file="Anaconda3-2022*.sh"
ret=$(find . -type f -iname "$file")

# Avoir un nom de fichier propre a la fin
# avoir uniquement le nom du fichier

if [[ -d $HOME/anacondaTer ]]
 then
   sudo rm -r $HOME/anacondaTer
fi
finalName=$(basename $ret)
echo "$finalName"

# shellcheck disable=SC1072
# tester si il trouve le fichier il l'éxacute
if test ! -z "$finalName"; then
    echo "le fichier existe"
    # Donner les permissions au fichier
    sudo chmod 755 $finalName
    #exaction du programme anaconda et créer un répertoire anacondaTer pour notre projet
    sudo ./$finalName -b -p  $HOME/anacondaTer

#sinon on le telecharge sur le site de Anaconda
elif   sudo wget https://repo.anaconda.com/archive/Anaconda3-2022.05-Linux-x86_64.sh ; then
    echo " ${green}Le ficher Anaconda3-2022.05-Linux-x86_64.sh a été  téléchargé ${res}"
    # Donner les permissions au fichier
    sudo chmod +x Anaconda3-2022.05-Linux-x86_64.sh
    sudo ./Anaconda3-2022.05-Linux-x86_64.sh -b -p $HOME/anacondaTer
fi

# #fin setup1