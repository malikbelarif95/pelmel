Extracteur d'entitées nommées est une application qui permet d'extraire les noms de personnes et les organismes

## Etape 1.

    Créer un repertoir pour votre projet avec 
    mkdir monRepertoir 
    cd /chemin/vers/monRepertoir

## Etape 2.

    Telechargez le projet en format zip  depuis https://gitlab.com/malikbelarif95/pelmel/-/archive/main/pelmel-main.zip et decompressé le dans le repertoire
    Puis cd PelMel (ou le nouveau nom du fichier zip si vous le renommez )

## Etape 3.

    Exacutez le script setup1.sh avec
    sudo ./setup1.sh OU sudo /bin/bash setup1.sh  

## Etape 4.

    Fermez la console et ouvrez une de nouveau 
    Ensuit :
    cd /chemin/vers/MonRepertoir/PelMel
    Puis les commandes 
    
    conda create --name my_env python=3.7

    conda update -n my_env scipy

    conda config --append envs_dirs $HOME/anacondaTer/envs

    conda activate my_env


## Etape 5.

    Exacutez le script setup2.sh
    sudo ./setup2.sh OU sudo /bin/bash setup2.sh 


## Etape 6.
    
    Exacutez : sudo python3.7 main.py

### Remarque 

    A chaque utilisation lors du redemarage de la machine ou bien l'ouverture d'un nouveau terminal il faut faire 
    conda activate my_env
    puis exacutez : sudo python3.7 main.py







