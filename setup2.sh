#!/bin/bash

echo $HOME
#Couleurs Pour les affichages
red=`tput setaf 1`
green=`tput setaf 2`
blue=`tput setaf 4`
res=`tput sgr0`

# Liste des library python utilise
pipLibrary="numpy pandas tk tika regex ttkthemes git"


#Verifier si l'internet et fonctionelle
if ping -q -c 1 -W 1 google.com >/dev/null; then
    echo "${green}The network is up${res}"


#Verifier le type d'OS Linus ou Windows
  platform='unknown'
  unamestr=$(uname)
    if [[ "$unamestr" == 'Linux' ]]; then
        platform='Linux'
    elif [[ "$unamestr" == 'Windows' ]]; then
        platform='Windows'
    fi

        #Verification et installation des library python via conda
        for v in $pipLibrary;

            do
                echo "Debut instalaltion de ${blue}$v${res}"
                if conda install $v ; then
                echo "Library ${green}$v${res} is already installed"
            else
                conda install -y $v
            fi
        done
            echo "Debut instalaltion de ${blue}Spacy${res}"
            if conda install -c conda-forge spacy -y  ; then
                echo "Library ${green}sapcy ${res} is already installed"
            fi

            echo "Debut instalaltion de ${blue}spacy-model-fr_core_news_md${res}"
            if  conda install -c conda-forge spacy-model-fr_core_news_md -y ; then
                echo "Library ${green}spacy-model-fr_core_news_md${res} is already installed"
            fi
        pip install tika
        pip installt ttkthemes
else
    echo "${red}The network is down${res}"
fi
