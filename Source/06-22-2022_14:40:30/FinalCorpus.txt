Discours

Discours

de Madame Roselyne BACHELOT-NARQUIN

Ministre des Solidarités et de la Cohésion sociale

-----------

REMISE

du PRIX d’HONNEUR
à l’ÉQUIPE de FRANCE HANDISPORT
------------

-Mardi 23 Novembre 2010-

à 20h10
(CARROUSEL du LOUVRE)

99, rue de Rivoli

Paris 1er
Mesdames, messieurs,

C’est à moi que revient le plaisir de remettre le Prix d’honneur ce soir, décerné cette année à l’ensemble de l’équipe de France d’escrime handisport.
Vous l’imaginez bien, ce n’est pas pour déplaire à l’ancienne Ministre des sports que je suis !

A l’équipe d’épée, qui représente ici tous les athlètes, à ses remarquables ambassadeurs, David Maillard, Marc-André Cratère, Robert Citerne et Romain Noble (absent), j’adresse toutes mes félicitations.

Vous l’avez une nouvelle fois montré lors des récents championnats du monde d’escrime qui se sont tenus au Grand Palais : vous êtes des sportifs exceptionnels.

La moisson de 11 médailles que vous avez récoltées l’atteste à elle seule.

Avec 3 médailles d’or – dont celle de l’équipe épée –, 1 d’argent et 7 de bronze, vous avez classé la France au troisième rang des Nations, avec le même nombre total de médailles que la Chine qui, elle, figure au premier rang.

C’est un excellent bilan et vous pouvez être fiers de vous, comme nous le sommes.

Dans ce superbe palmarès, je vois un signe particulièrement encourageant, moins de deux ans des Jeux paralympiques de Londres.

J’en profite d’ailleurs pour remercier le président de la Fédération handisport, Gérard Masson, pour son action déterminante dans cette belle aventure.

Au-delà de la formidable performance sportive que vous avez accomplie, c’est aussi notre regard à toutes et à tous que vous avez contribué à faire évoluer.

Bravo à toutes et à tous !

1
3
Discours

Discours

de Madame Roselyne BACHELOT-NARQUIN

Ministre des Solidarités et de la Cohésion sociale

-----------

REMISE

du PRIX d’HONNEUR
à l’ÉQUIPE de FRANCE HANDISPORT
------------

-Mardi 23 Novembre 2010-

à 20h10
(CARROUSEL du LOUVRE)

99, rue de Rivoli

Paris 1er
Mesdames, messieurs,

C’est à moi que revient le plaisir de remettre le Prix d’honneur ce soir, décerné cette année à l’ensemble de l’équipe de France d’escrime handisport.

Vous l’imaginez bien, ce n’est pas pour déplaire à l’ancienne Ministre des sports que je suis !

A l’équipe d’épée, qui représente ici tous les athlètes, à ses remarquables ambassadeurs, David Maillard, Marc-André Cratère, Robert Citerne et Romain Noble (absent), j’adresse toutes mes félicitations.

Vous l’avez une nouvelle fois montré lors des récents championnats du monde d’escrime qui se sont tenus au Grand Palais : vous êtes des sportifs exceptionnels.

La moisson de 11 médailles que vous avez récoltées l’atteste à elle seule.

Avec 3 médailles d’or – dont celle de l’équipe épée –, 1 d’argent et 7 de bronze, vous avez classé la France au troisième rang des Nations, avec le même nombre total de médailles que la Chine qui, elle, figure au premier rang.

C’est un excellent bilan et vous pouvez être fiers de vous, comme nous le sommes.

Dans ce superbe palmarès, je vois un signe particulièrement encourageant, moins de deux ans des Jeux paralympiques de Londres.

J’en profite d’ailleurs pour remercier le président de la Fédération handisport, Gérard Masson, pour son action déterminante dans cette belle aventure.

Au-delà de la formidable performance sportive que vous avez accomplie, c’est aussi notre regard à toutes et à tous que vous avez contribué à faire évoluer.

Bravo à toutes et à tous !

PAGE  
4
