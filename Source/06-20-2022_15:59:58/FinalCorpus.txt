Discours

Discours

de Madame Roselyne BACHELOT-NARQUIN

Ministre des Solidarités et de la Cohésion sociale
----------
OUVERTURE du COLLOQUE
organisé par l’AP-DA
(Association des Directeurs au service des Personnes Âgées)

----------

-Lundi 21 Mars 2011-

à 14h05
(PORTE MAILLOT)

Grand Amphithéâtre
Monsieur le président, cher Pascal Champvert,

Mesdames et messieurs les directeurs,

Mesdames, Messieurs,

A l’heure du grand débat national sur la dépendance, j’ai tenu à intervenir à votre congrès, et je vous remercie, cher Pascal Champvert, de m’en offrir l’occasion.
Quel est le sens de ce débat, en effet, sinon interroger la place des personnes âgées dans notre société et réfléchir à notre système de prise en charge de la dépendance ?

Vous qui, en établissement ou à domicile, travaillez au plus près des personnes âgées, vous qui connaissez parfaitement leurs besoins et leurs attentes, savez combien cette réflexion est nécessaire.
Elle est indispensable si nous voulons apporter, pour aujourd’hui et pour demain, des réponses concrètes aux difficultés d’ordre pratique, psychologique, affectif ou financier rencontrées par les personnes âgées dépendantes et par leurs proches, mais aussi aux difficultés des professionnels.

Et elle est primordiale si nous voulons, plus largement, faire évoluer notre regard sur le vieillissement. J’y reviendrai tout à l’heure.

*

Si le Président de la République a souhaité le lancement d’un débat national sur cette question d’envergure, c’est bien pour aborder les enjeux tout à la fois humains, sociaux, financiers et éthiques de la dépendance.

L’organisation de ce débat vise ainsi à identifier les bonnes pratiques et les bonnes idées. Pour ce faire, tous nos concitoyens sont invités à participer à la réflexion, parce qu’ils sont tous ou seront tous un jour concernés, en tant que personnes dépendantes ou en tant que proches.

Vous le savez, j’ai installé, en janvier, 4 groupes de travail thématiques, destinés à dresser un état des lieux de la situation actuelle et à établir des propositions concrètes.

Leurs travaux avancent bien ; les modérateurs de ces groupes m’en rendent compte régulièrement.
Mais aux côtés des experts, tous ceux qui le souhaitent sont invités à prendre part au débat.

Dès le 18 avril, et jusqu’au mois de juin, se tiendront les débats régionaux. Suivant une logique participative, ils permettront à nos concitoyens, mieux informés, de s’exprimer, partout en France.

4 colloques interrégionaux auront également lieu sur les thématiques abordées dans les groupes de travail. Ils seront précédés de la mise en place de « groupes de parole » de citoyens, qui favoriseront, une fois encore, les échanges.

Un site Internet dédié au débat national a, enfin, été lancé (www.dependance.gouv.fr). Nous avons déjà reçu des contributions, la plupart venant de femmes, ce qui ne vous étonnera pas !
Beaucoup d’entre elles insistent en particulier sur le rôle des aidants et la nécessité de mieux les soutenir. L’importance de la solidarité, celle de la prévention et la nécessité de définir, avant toute décision, un choix de société se dessinent également très nettement.

Faut-il le préciser ? Je vous invite, toutes et tous, à enrichir les débats et le site Internet de vos propres contributions.
Je connais en particulier, cher Pascal Champvert, votre créativité et votre énergie infatigable pour rendre vos établissements, ainsi que ceux du réseau de l’AD-PA, plus dynamiques, plus ouverts et mieux adaptés. Le nombre d’innovations que, le premier, vous avez mises en place le prouve assez.
Je ne doute pas qu’avec l’ensemble des directeurs et des professionnels que vous représentez vous aurez à cœur de contribuer à la réforme, par des propositions innovantes, basées sur votre expertise.

*

La réflexion est donc lancée.

Avec Marie-Anne Montchamp, nous avons par ailleurs reçu les principaux responsables politiques, représentants syndicaux, membres de fédérations et associations du secteur médico-social, ainsi que les représentants des principaux cultes et courants de pensée. Il nous a semblé important, en effet, de recueillir leurs attentes et leurs points de vue.

De ces entretiens et éléments de réflexion, issus des groupes et des premières contributions, quelques grandes lignes ressortent tout particulièrement.

L’importance, d’abord, du libre choix entre le maintien à domicile et la prise en charge par des établissements. De ce libre choix, vous-mêmes êtes les défenseurs les plus ardents !

La nécessité, ensuite, d’être vigilants sur la qualité des prises en charge. 

Dans ce domaine, les questions de la bientraitance et de la place des familles, que vous avez abordées et aborderez aujourd’hui, sont évidemment centrales.

La bientraitance, en effet, n’est autre qu’une prise en charge de qualité, respectueuse de l’intégrité et de la singularité de chacun.

Plusieurs orientations ont été définies pour lutter contre la maltraitance et promouvoir la bientraitance.

Chacun le sait, la maltraitance à domicile est majoritaire, bien que moins bien connue. Plus difficile à dénoncer pour les victimes, parce qu’elle met en jeu des liens familiaux ancrés, des chantages affectifs, des sentiments de culpabilité et la peur de représailles, elle doit être combattue avec force.
Aussi est-il important de mieux faire connaître le numéro national de signalement de la maltraitance (3977), mis en place en 2008, et de lever les freins au signalement.

En outre, depuis sa création en 2003, le Comité national de vigilance contre la maltraitance des personnes âgées – élargi, en 2007, aux adultes handicapés – est chargé de définir les outils pour la mise en œuvre de la politique nationale.
2 guides de gestion des risques de maltraitance – l’un destiné aux établissements d’hébergement, l’autre aux services d’aide à domicile – ont ainsi été réalisés. Je suis certaine que vous en avez pris connaissance.
L’accompagnement des EHPAD dans une dynamique de bientraitance s’effectue également par le biais de la formation : la formation bientraitance a, par exemple, été généralisée sur la base des outils Mobiqual, dans le cadre d’une convention avec la CNSA.

Par ailleurs, l’Agence nationale de l’évaluation et de la qualité des établissements et services sociaux et médico-sociaux (ANESM) a diffusé dans les EHPAD, en 2009, un outil d’auto-évaluation de la bientraitance, à partir de sa recommandation sur la même thématique.
Enfin, l’Agence a relayé, en 2010, l’engagement pris par le Président de la République, lors de l’anniversaire du plan Alzheimer, de promouvoir les pratiques concourant à l’amélioration de la qualité de vie des personnes âgées en EHPAD.

Des recommandations s’articulent autour de 4 enjeux majeurs :

- l’accueil de la personne âgée et son accompagnement ;

- l’organisation du cadre de vie et de la vie quotidienne ;

- la vie sociale des résidents ;

- l’impact des éléments de santé sur la qualité de vie.

Les trois premiers domaines visent très nettement à mettre la personne âgée et sa famille au centre de la prise en charge.

Car l’entrée en EHPAD ne va pas de soi, ni pour la personne âgée, ni pour sa famille. Elle doit donc se préparer, afin de créer l’adhésion du futur résident à son nouvel environnement de vie.
L’intégration, dès le début, de l’entourage familial semble indispensable. Qui d’autre que la famille connaît mieux le mode de vie, les besoins et les attentes du futur résident ? Pourquoi le rôle de la famille s’arrêterait-il aux portes de l’EHPAD ?
Les familles peuvent participer à la vie quotidienne du parent ou conjoint résident : faire sa toilette, l’aider pour ses repas, l’accompagner dans ses sorties…
Les familles et leurs associations doivent, en outre, jouer un rôle à la hauteur de leur engagement dans les conseils de vie sociale, qui peuvent devenir de véritables lieux d’expression, d’échange et de collaboration avec les professionnels et les directions. Je souhaite que ces conseils se généralisent et fassent pleinement participer les familles.

J’aimerais d’ailleurs saluer l’engagement de la Fédération nationale des associations de personnes âgées et de leurs familles (FNAPAEF) et de sa présidente, Joëlle Le Gall, sur ces sujets.

En établissement comme à domicile, la place de l’entourage est indéniable.

Dans les différentes contributions au débat, le rôle, précieux, des aidants et des familles fait effectivement largement consensus. Comment pourrait-il en être autrement ?

La réforme devra permettre de mieux le valoriser.

Car si l’Etat ne peut pas et ne doit pas se substituer aux solidarités familiales ou aux solidarités de proximité, son rôle est de les soutenir pour garantir leur pérennité.

Il est indéniable qu’aujourd’hui on observe une plus grande complémentarité entre aidants et professionnels. C’est d’ailleurs, dans certains cas, l’une des façons de préserver la qualité de la relation qui unit l’aidant à son proche dépendant, grâce au répit qui lui est accordé.

J’aimerais, à ce sujet, saluer l’action des services à domicile, dont je n’ignore pas les difficultés. Le groupe de travail qui, sous l’égide d’Evelyne Ratte, est chargé d’étudier l’ « Accueil et [l’]accompagnement des personnes âgées », doit me rendre des propositions.

Plusieurs pistes se dessinent :

· aider les conseils généraux, qui ne disposent pas toujours des bons outils de pilotage. Le rapport de l’Inspection  générale des affaires sociales (IGAS), qui vient d’être publié, préconise la mise en place d’un référentiel d’analyse des coûts de revient des services, commun à l’ensemble des conseils généraux. Cette idée mérite d’être approfondie ;

· nous devons également réfléchir à une amélioration du mode d’allocation des ressources, et donc du mode de tarification. Deux projets sont sur la table : celui de l’Assemblée des départements de France (ADF) et celui de l’IGAS.
Ils seront évidemment discutés avec tous les acteurs du secteur. Notre objectif est la suivant : pérenniser des structures qui assurent une mission essentielle, en essayant de conserver la diversité de l’offre, allant du public au privé lucratif, en passant par l’associatif.

*

La dernière grande ligne qui ressort, à ce stade, de la réflexion concerne le financement de la prise en charge de la dépendance, et notamment l’impératif de responsabilité. Le Président de la République a très clairement énoncé qu’il refusait la voie du déficit et de l’endettement.

Il a également écarté une autre voie : celle qui consisterait à taxer davantage le travail. Pour le reste, le débat est ouvert.
La question du financement de l’aide aux personnes âgées fait évidemment partie de vos interrogations pour ce congrès.

J’aimerais en profiter pour évoquer la campagne budgétaire qui était intervenue l’an dernier trop tardivement.
Je m’engage, cette année, à la démarrer au plus vite, dès le mois d’avril, dans la foulée du conseil de la CNSA du 12 avril prochain, qui permettra de répartir les montants de dépenses déterminant les enveloppes régionales.

Surtout, je rappellerai l’effort extrêmement important consenti dans le secteur de la prise en charge des personnes âgées. 348 millions d’euros supplémentaires seront ainsi alloués cette année.

Un dernier mot, enfin, sur le décret permettant une évolution de la tarification dans les EHPAD. Il a fait l’objet d’un grand nombre de discussions, qui ont amené les pouvoirs publics à le modifier plusieurs fois.
Deux éléments ont néanmoins amené le Gouvernement à en décaler la parution, comme je l’ai annoncé devant le Sénat :

· la généralisation du tarif intégral nécessitait de pouvoir s’appuyer sur des données précises et complètes. Or, celles-ci font encore défaut, notamment au niveau du montant à transférer entre l’enveloppe soins de ville et celle du médico-social en contrepartie de l’intégration des honoraires des généralistes et des prestations de biologie et de radiologie dans les forfaits intégraux des établissements. Une mission IGAS a été lancée pour apporter des éléments objectifs de réponse à ces questions ;

· deuxième raison : le lancement du grand débat national. En effet, l’une des priorités de la réforme de la dépendance est la diminution du reste à charge en établissement. Les discussions porteront donc nécessairement sur le mode de tarification des établissements. C’est pourquoi il aurait été inopportun de sortir un décret d’une telle importance en amont de la concertation.

*

Les enjeux financiers ne doivent pas pour autant faire oublier l’essentiel.
L’essentiel, c’est le regard que nous portons sur le vieillissement dans une société trop régie par le culte de la performance.

L’essentiel, c’est le lien que nous tissons entre les générations et la façon dont nous traitons nos aînés.

Car au-delà des compétences, si précieuses, de professionnels investis, c’est aussi d’accompagnement dont les personnes âgées ont besoin. Un accompagnement fait d’empathie, d’écoute et de douceur.

Car dans notre rapport aux personnes âgées, c’est également notre rapport à la vulnérabilité et à la finitude qui se joue.

Comment, jusqu’au bout de la vie, permettre à une personne d’être avant tout traitée comme une personne humaine ? Ne pas lui imposer des décisions qu’elle n’a pas choisies, tenir compte de son histoire, l’aider à définir un projet de vie : autant de façons d’accompagner une personne âgée, dans le respect de sa dignité.

Comment lui permettre de se sentir pleinement intégrée à notre pacte social ? La citoyenneté ne saurait décroître à mesure que les forces faiblissent.

Toutes ces questions méritent d’être posées.

Cela, Geneviève Laroque, Marie-Françoise Fuchs et Fabrice Gzil ne manqueront pas de nous le rappeler dans un instant.

9
2
Discours

Discours

de Madame Roselyne BACHELOT-NARQUIN

Ministre des Solidarités et de la Cohésion sociale
----------
OUVERTURE du COLLOQUE
organisé par l’AP-DA
(Association des Directeurs au service des Personnes Âgées)

----------

-Lundi 21 Mars 2011-

à 14h05
(PORTE MAILLOT)

Grand Amphithéâtre
Monsieur le président, cher Pascal Champvert,

Mesdames et messieurs les directeurs,

Mesdames, Messieurs,

A l’heure du grand débat national sur la dépendance, j’ai tenu à intervenir à votre congrès, et je vous remercie, cher Pascal Champvert, de m’en offrir l’occasion.

Quel est le sens de ce débat, en effet, sinon interroger la place des personnes âgées dans notre société et réfléchir à notre système de prise en charge de la dépendance ?

Vous qui, en établissement ou à domicile, travaillez au plus près des personnes âgées, vous qui connaissez parfaitement leurs besoins et leurs attentes, savez combien cette réflexion est nécessaire.

Elle est indispensable si nous voulons apporter, pour aujourd’hui et pour demain, des réponses concrètes aux difficultés d’ordre pratique, psychologique, affectif ou financier rencontrées par les personnes âgées dépendantes et par leurs proches, mais aussi aux difficultés des professionnels.

Et elle est primordiale si nous voulons, plus largement, faire évoluer notre regard sur le vieillissement. J’y reviendrai tout à l’heure.

*

Si le Président de la République a souhaité le lancement d’un débat national sur cette question d’envergure, c’est bien pour aborder les enjeux tout à la fois humains, sociaux, financiers et éthiques de la dépendance.

L’organisation de ce débat vise ainsi à identifier les bonnes pratiques et les bonnes idées. Pour ce faire, tous nos concitoyens sont invités à participer à la réflexion, parce qu’ils sont tous ou seront tous un jour concernés, en tant que personnes dépendantes ou en tant que proches.

Vous le savez, j’ai installé, en janvier, 4 groupes de travail thématiques, destinés à dresser un état des lieux de la situation actuelle et à établir des propositions concrètes.

Leurs travaux avancent bien ; les modérateurs de ces groupes m’en rendent compte régulièrement.

Mais aux côtés des experts, tous ceux qui le souhaitent sont invités à prendre part au débat.

Dès le 18 avril, et jusqu’au mois de juin, se tiendront les débats régionaux. Suivant une logique participative, ils permettront à nos concitoyens, mieux informés, de s’exprimer, partout en France.

4 colloques interrégionaux auront également lieu sur les thématiques abordées dans les groupes de travail. Ils seront précédés de la mise en place de « groupes de parole » de citoyens, qui favoriseront, une fois encore, les échanges.

Un site Internet dédié au débat national a, enfin, été lancé (www.dependance.gouv.fr). Nous avons déjà reçu des contributions, la plupart venant de femmes, ce qui ne vous étonnera pas !

Beaucoup d’entre elles insistent en particulier sur le rôle des aidants et la nécessité de mieux les soutenir. L’importance de la solidarité, celle de la prévention et la nécessité de définir, avant toute décision, un choix de société se dessinent également très nettement.

Faut-il le préciser ? Je vous invite, toutes et tous, à enrichir les débats et le site Internet de vos propres contributions.

Je connais en particulier, cher Pascal Champvert, votre créativité et votre énergie infatigable pour rendre vos établissements, ainsi que ceux du réseau de l’AD-PA, plus dynamiques, plus ouverts et mieux adaptés. Le nombre d’innovations que, le premier, vous avez mises en place le prouve assez.

Je ne doute pas qu’avec l’ensemble des directeurs et des professionnels que vous représentez vous aurez à cœur de contribuer à la réforme, par des propositions innovantes, basées sur votre expertise.

*

La réflexion est donc lancée.

Avec Marie-Anne Montchamp, nous avons par ailleurs reçu les principaux responsables politiques, représentants syndicaux, membres de fédérations et associations du secteur médico-social, ainsi que les représentants des principaux cultes et courants de pensée. Il nous a semblé important, en effet, de recueillir leurs attentes et leurs points de vue.

De ces entretiens et éléments de réflexion, issus des groupes et des premières contributions, quelques grandes lignes ressortent tout particulièrement.

L’importance, d’abord, du libre choix entre le maintien à domicile et la prise en charge par des établissements. De ce libre choix, vous-mêmes êtes les défenseurs les plus ardents !

La nécessité, ensuite, d’être vigilants sur la qualité des prises en charge.
Dans ce domaine, les questions de la bientraitance et de la place des familles, que vous avez abordées et aborderez aujourd’hui, sont évidemment centrales.

La bientraitance, en effet, n’est autre qu’une prise en charge de qualité, respectueuse de l’intégrité et de la singularité de chacun.

Plusieurs orientations ont été définies pour lutter contre la maltraitance et promouvoir la bientraitance.

Chacun le sait, la maltraitance à domicile est majoritaire, bien que moins bien connue. Plus difficile à dénoncer pour les victimes, parce qu’elle met en jeu des liens familiaux ancrés, des chantages affectifs, des sentiments de culpabilité et la peur de représailles, elle doit être combattue avec force.

Aussi est-il important de mieux faire connaître le numéro national de signalement de la maltraitance (3977), mis en place en 2008, et de lever les freins au signalement.

En outre, depuis sa création en 2003, le Comité national de vigilance contre la maltraitance des personnes âgées – élargi, en 2007, aux adultes handicapés – est chargé de définir les outils pour la mise en œuvre de la politique nationale.

2 guides de gestion des risques de maltraitance – l’un destiné aux établissements d’hébergement, l’autre aux services d’aide à domicile – ont ainsi été réalisés. Je suis certaine que vous en avez pris connaissance.

L’accompagnement des EHPAD dans une dynamique de bientraitance s’effectue également par le biais de la formation : la formation bientraitance a, par exemple, été généralisée sur la base des outils Mobiqual, dans le cadre d’une convention avec la CNSA.

Par ailleurs, l’Agence nationale de l’évaluation et de la qualité des établissements et services sociaux et médico-sociaux (ANESM) a diffusé dans les EHPAD, en 2009, un outil d’auto-évaluation de la bientraitance, à partir de sa recommandation sur la même thématique.

Enfin, l’Agence a relayé, en 2010, l’engagement pris par le Président de la République, lors de l’anniversaire du plan Alzheimer, de promouvoir les pratiques concourant à l’amélioration de la qualité de vie des personnes âgées en EHPAD.

Des recommandations s’articulent autour de 4 enjeux majeurs :

- l’accueil de la personne âgée et son accompagnement ;

- l’organisation du cadre de vie et de la vie quotidienne ;

- la vie sociale des résidents ;

- l’impact des éléments de santé sur la qualité de vie.

Les trois premiers domaines visent très nettement à mettre la personne âgée et sa famille au centre de la prise en charge.

Car l’entrée en EHPAD ne va pas de soi, ni pour la personne âgée, ni pour sa famille. Elle doit donc se préparer, afin de créer l’adhésion du futur résident à son nouvel environnement de vie.

L’intégration, dès le début, de l’entourage familial semble indispensable. Qui d’autre que la famille connaît mieux le mode de vie, les besoins et les attentes du futur résident ? Pourquoi le rôle de la famille s’arrêterait-il aux portes de l’EHPAD ?

Les familles peuvent participer à la vie quotidienne du parent ou conjoint résident : faire sa toilette, l’aider pour ses repas, l’accompagner dans ses sorties…

Les familles et leurs associations doivent, en outre, jouer un rôle à la hauteur de leur engagement dans les conseils de vie sociale, qui peuvent devenir de véritables lieux d’expression, d’échange et de collaboration avec les professionnels et les directions.

Je souhaite que ces conseils se généralisent et fassent pleinement participer les familles.

J’aimerais d’ailleurs saluer l’engagement de la Fédération nationale des associations de personnes âgées et de leurs familles (FNAPAEF) et de sa présidente, Joëlle Le Gall, sur ces sujets.

En établissement comme à domicile, la place de l’entourage est indéniable.

Dans les différentes contributions au débat, le rôle, précieux, des aidants et des familles fait effectivement largement consensus. Comment pourrait-il en être autrement ?

La réforme devra permettre de mieux le valoriser.

Car si l’Etat ne peut pas et ne doit pas se substituer aux solidarités familiales ou aux solidarités de proximité, son rôle est de les soutenir pour garantir leur pérennité.

Il est indéniable qu’aujourd’hui on observe une plus grande complémentarité entre aidants et professionnels. C’est d’ailleurs, dans certains cas, l’une des façons de préserver la qualité de la relation qui unit l’aidant à son proche dépendant, grâce au répit qui lui est accordé.

J’aimerais, à ce sujet, saluer l’action des services à domicile, dont je n’ignore pas les difficultés.

Le groupe de travail qui, sous l’égide d’Evelyne 
Ratte, est chargé d’étudier l’ « Accueil et [l’]accompagnement des personnes âgées », doit me rendre des propositions.

Plusieurs pistes se dessinent :

· aider les conseils généraux, qui ne disposent pas toujours des bons outils de pilotage. Le rapport de l’Inspection  générale des affaires sociales (IGAS), qui vient d’être publié, préconise la mise en place d’un référentiel d’analyse des coûts de revient des services, commun à l’ensemble des conseils généraux. Cette idée mérite d’être approfondie ;

· nous devons également réfléchir à une amélioration du mode d’allocation des ressources, et donc du mode de tarification. Deux projets sont sur la table : celui de l’Assemblée des départements de France (ADF) et celui de l’IGAS.

Ils seront évidemment discutés avec tous les acteurs du secteur. Notre objectif est la suivant : pérenniser des structures qui assurent une mission essentielle, en essayant de conserver la diversité de l’offre, allant du public au privé lucratif, en passant par l’associatif.

*

La dernière grande ligne qui ressort, à ce stade, de la réflexion concerne le financement de la prise en charge de la dépendance, et notamment l’impératif de responsabilité. Le Président de la République a très clairement énoncé qu’il refusait la voie du déficit et de l’endettement.

Il a également écarté une autre voie : celle qui consisterait à taxer davantage le travail. Pour le reste, le débat est ouvert.

La question du financement de l’aide aux personnes âgées fait évidemment partie de vos interrogations pour ce congrès.

J’aimerais en profiter pour évoquer la campagne budgétaire qui était intervenue l’an dernier trop tardivement.

Je m’engage, cette année, à la démarrer au plus vite, dès le mois d’avril, dans la foulée du conseil de la CNSA du 12 avril prochain, qui permettra de répartir les montants de dépenses déterminant les enveloppes régionales.

Surtout, je rappellerai l’effort extrêmement important consenti dans le secteur de la prise en charge des personnes âgées. 348 millions d’euros supplémentaires seront ainsi alloués cette année.

Un dernier mot, enfin, sur le décret permettant une évolution de la tarification dans les EHPAD. Il a fait l’objet d’un grand nombre de discussions, qui ont amené les pouvoirs publics à le modifier plusieurs fois.

Deux éléments ont néanmoins amené le Gouvernement à en décaler la parution, comme je l’ai annoncé devant le Sénat :

· la généralisation du tarif intégral nécessitait de pouvoir s’appuyer sur des données précises et complètes. Or, celles-ci font encore défaut, notamment au niveau du montant à transférer entre l’enveloppe soins de ville et celle du médico-social en contrepartie de l’intégration des honoraires des généralistes et des prestations de biologie et de radiologie dans les forfaits intégraux des établissements. Une mission IGAS a été lancée pour apporter des éléments objectifs de réponse à ces questions ;

· deuxième raison : le lancement du grand débat national. En effet, l’une des priorités de la réforme de la dépendance est la diminution du reste à charge en établissement. Les discussions porteront donc nécessairement sur le mode de tarification des établissements. C’est pourquoi il aurait été inopportun de sortir un décret d’une telle importance en amont de la concertation.

*

Les enjeux financiers ne doivent pas pour autant faire oublier l’essentiel.

L’essentiel, c’est le regard que nous portons sur le vieillissement dans une société trop régie par le culte de la performance.

L’essentiel, c’est le lien que nous tissons entre les générations et la façon dont nous traitons nos aînés.

Car au-delà des compétences, si précieuses, de professionnels investis, c’est aussi d’accompagnement dont les personnes âgées ont besoin. Un accompagnement fait d’empathie, d’écoute et de douceur.

Car dans notre rapport aux personnes âgées, c’est également notre rapport à la vulnérabilité et à la finitude qui se joue.

Comment, jusqu’au bout de la vie, permettre à une personne d’être avant tout traitée comme une personne humaine ? Ne pas lui imposer des décisions qu’elle n’a pas choisies, tenir compte de son histoire, l’aider à définir un projet de vie : autant de façons d’accompagner une personne âgée, dans le respect de sa dignité.

Comment lui permettre de se sentir pleinement intégrée à notre pacte social ? La citoyenneté ne saurait décroître à mesure que les forces faiblissent.

Toutes ces questions méritent d’être posées.

Cela, Geneviève Laroque, Marie-Françoise Fuchs et Fabrice Gzil ne manqueront pas de nous le rappeler dans un instant.

24
2
